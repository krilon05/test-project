var gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    jeet  = require('jeet'),
    rupture = require('rupture'),
    nib = require('nib'),
    typographic = require('typographic'),
    minifyCSS = require('gulp-minify-css'),
    axis = require('axis')
    ;

gulp.task('default', function() {
    // stylus
    gulp.src('./stylus/main.styl')
        .pipe(stylus({error: true, use: [nib(),jeet(),rupture(),typographic(),axis()]}))
        .pipe(minifyCSS())
        .pipe(gulp.dest('./css/'));
});